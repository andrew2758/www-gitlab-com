# Tech Stack - Add New System & System Onboarding
**Please do not merge before the Business Systems Analysts have reviewed and approved!**

**Questions? Ask in [#tech-owners_tech-stack](https://gitlab.enterprise.slack.com/archives/C027X43TLCE) Slack channel.**

## Business/Technical System Owner or Delegate to Complete
* [ ] Rename this MR's title to `[System Name] - Tech Stack - Add New System & System Onboarding`

**General Tech Stack Entry Tasks**
1. [ ] Requisition Link (if an externally-developed System):
2. [ ] Populate all data fields within the 'Changes' tab of this MR. More instructions are [here](https://about.gitlab.com/handbook/business-technology/tech-stack-applications/#what-data-lives-in-the-tech-stack).
3. [ ] Check this box to **indicate approval** of the New System's [Critical System Tier](https://handbook.gitlab.com/handbook/security/security-assurance/security-risk/storm-program/critical-systems/#designating-critical-system-tiers).
4. Is this New System replacing an existing System in the Tech Stack? 
    * [ ] Yes - Delete the existing System's entry from the Tech Stack within this MR. Next, create a [Tech Stack Offboarding Issue](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/new?issuable_template=offboarding_tech_stack).  Offboarding Issue Link:
    * [ ] No

**Access Tasks**
1. [ ] [Create an Issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Update_Tech_Stack_Provisioner) to add the Provisioner(s) of the New System to the appropriate Google/Slack/GitLab groups. _Note: If the Provisioner(s) of this System is already part of the Provisioner groups, skip this step. Please replace the link placeholder below with `N/A - Already in Provisioner groups`_.
    * Issue Link:
2. [ ] Add the New System to **one** of two Offboarding templates below. More instructions are [here](https://about.gitlab.com/handbook/business-technology/tech-stack-applications/#updating-the-offboarding-templates).
    * [ ] Option 1 - Main [Team Member Offboarding](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/main/.gitlab/issue_templates/offboarding.md) template
      * MR Link:
    * [ ] Option 2 - [Department-level Offboarding template](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/tree/main/.gitlab/issue_templates/offboarding_tasks) folder
      * MR Link:

**System Onboarding Checklist**
1. Is the New System configured for [Okta Single Sign On](https://handbook.gitlab.com/handbook/business-technology/okta/#how-is-gitlab-using-okta)?
    * [ ] Yes
    * [ ] No.  Please complete this [Issue template](https://gitlab.com/gitlab-com/business-technology/change-management/-/issues/new?issuable_template=okta_new_app_request). 
      * Okta SSO Issue Link:
2. Is encryption of data in-transit and data at-rest enabled for the New System?
    * [ ] Yes
    * [ ] No
3. Do System administrators (GitLab Team Members) have access to system logs?
    * [ ] Yes
    * [ ] No
4. Have all SOC 2 CUECs been reviewed and implemented (as applicable)?
    * [ ] Yes - Link to Comment in TPRM Assessment Report Issue indicating confirmation from Business Team:
    * [ ] No
    * [ ] N/A
5. Please review the following items:
    * Security Compliance ['General Controls Checklist'](https://gitlab.com/gitlab-com/gl-security/security-assurance/team-commercial-compliance/compliance/-/issues/3418#top). Please read this Issue to gain an understanding of the types of controls that are required if this New System was to be "in-scope" for one of GitLab's external certifications (e.g. [SOC](https://www.aicpa-cima.com/topic/audit-assurance/audit-and-assurance-greater-than-soc-2), [SOX](https://us.aicpa.org/advocacy/issues/section404bofsox#:~:text=The%20Sarbanes%2DOxley%20Act%20requires,assessment%20of%20its%20internal%20controls.), [ISO](https://www.iso.org/standard/27001)).
    * [Current list of systems "in-scope"](https://docs.google.com/spreadsheets/d/11_53R0QQLes0ZftjMlWwhyOUqhhuZFPcbVunkHNeF-U/edit#gid=0), for reference.
      * [ ] I confirm review of both items.

## Privacy Team to Complete
If the New System contains [Personal Data](https://about.gitlab.com/handbook/security/data-classification-standard.html#data-classification-definitions), has a [Privacy Review been completed?](https://about.gitlab.com/handbook/legal/privacy/#privacy-review-process): 
* [ ] If System contains Orange (internal only) / RED Personal Data:
    * [ ] Yes - Link a completed Privacy Review Issue, Coupa approval, or Zip approval.
    * [ ] No - **Complete [Privacy Review Issue](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/new?issuable_template=Vendor-Procurement-Privacy-Review)**
* [ ] If System contains Yellow Personal Data (GitLab Team Member Names/Emails):
    * [ ] Yes - a Data Processing Agreement (DPA) was executed between GitLab and the Vendor.
    * [ ] No - a DPA is not in place. Privacy Team will be in contact about completing a DPA, which is required for this Tech Stack Addition.
* [ ] If System contains only Green Data or contains no [Personal Data](https://about.gitlab.com/handbook/security/data-classification-standard.html#data-classification-definitions), a Privacy Review is not required.

## Security Logging Team to Complete
* [ ] @gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging acknowledges the New System is appropriately logged.

## Security Risk Team to Complete
1. Answer Question 4. in 'System Onboarding Checklist' section above.
2. Was a [Technical Security Validation](https://handbook.gitlab.com/handbook/security/security-assurance/security-risk/third-party-risk-management/#technical-security-validations) launched in response to the TPRM Assessment?
   * [ ] Yes - Link the TSV `here` and confirm all steps within the **Observation Management** section of the TSV have been completed, including acknowledgment of TSV findings by the Business Owner if findings were noted.
   * [ ] No - No further action needed.

## Business Technology Team to Complete
* [ ] To-do before merging -- (@marc_disabatino) is to ensure all sections/action items are completed.

/cc @gitlab-com/internal-audit @disla
/assign @marc_disabatino @emccrann
/labels ~BusinessTechnology ~BT-TechStack ~BT-TechStack-NewSystem ~"BT-TechStack::To do" ~"TPRM::Unassigned"
