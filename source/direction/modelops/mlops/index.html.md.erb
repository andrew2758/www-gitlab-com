---
layout: sec_direction
title: "Group Direction - MLOps"
description: "Focused on enabling data teams to build, test, and deploy their machine learning models"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# MLOps

| | |
| --- | --- |
| Stage | [ModelOps](/direction/modelops/) |
| Maturity | [minimal](/direction/maturity/) |
| Content Last Reviewed | `2024-03-21` |


## Overview

Machine Learning Operations (MLOps) aims to automate building, experimenting, testing & evaluation, deployment and management of machine learning models in production reliably and efficiently. 

See brief overview video of why [MLOps is difficult](https://www.youtube.com/watch?v=n-stLxM3RSE).

## Vision

Data Scientist and ML Engineers work together in GitLab to build, experiment, deploy, monitor and keep models secure and up-to-date. Their processes are reproducible, automated, collaborative, scalable, and monitored. 

They further collaborate with other product development teams in GitLab so that there is tight coordination between models and their dependent applications. Teams stay informed on status of various components and can seamlessly coordinate making changes to production systems.

### Why is GitLab suitable for MLOPs?

Like software development, machine learning and model development require automation and collaboration to consistently and iteratively deliver value. As machine learning becomes more prevalent, the number of individuals, roles, and changes increases and it also overlaps more frequently with other software development areas. Instead of siloed workflows, bringing ML workflows into GitLab as a single collaboration platform extends the DevOps culture to data scientists, helping organizations achieve better results.

## Roadmap

Over the course of FY25, GitLab is ramping up a team dedicated to MLOps. We will focus on bringing the most basic and necessary MLOps capabilities to market. Namely, they are the ability to manage models, a model registry, and to deploy models to production.

Here's a [video](https://www.youtube.com/watch?v=tU-iu02pYXo) overview of what we have today.

### Integration with Notebooks

Data Scientist are not developers. They primarily work in notebooks. We want to make it easy for Data Scientist to store the models they're working on in the model registry by offering [integration with popular tools like Google Collab](https://gitlab.com/gitlab-org/gitlab/-/issues/448880).

### Mature Model Registry

The ability to manage a model within GitLab exists today. We plan to actively [mature](https://gitlab.com/groups/gitlab-org/-/epics/9423) the capabilities. We also plan to [merge model experiments and model registry](https://docs.gitlab.com/ee/architecture/blueprints/model_experiments_and_registry/) together.

### Model Deployment

A model repository is made more useful by adding the ability to deploy to production. With deployment, users can collaborate and complete a full basic ML workflow within GitLab.

## Auxiliary Information

### Sample ML Project Demos

Below are a few sample projects using GitLab CI and ML:

- [GitLab with Kubernetes for ML](https://gitlab.com/mon-customer-demo/imda-hybrid-coolness/imda-team-management/image-detection/experiment-datascientists/fashion-mnist-imda)
- [GitLab with Azure ML](https://gitlab.com/mon-customer-demo/imda-hybrid-coolness/imda-team-management/image-detection/experiment-datascientists/covmod)
- [GitLab with Kubeflow](https://gitlab.com/mon-customer-demo/car_crashes)

More reference implementation ideas can be found in the [MLOps with GitLab overview](https://datastudio.google.com/u/0/reporting/c708a721-2e95-409b-9a40-4c14eff1ae06/page/4OrWB).

### Analyst Research

Needs to be updated

* [2021 451 - The Current and Future State of AI and Machine Learning](https://web.451alliance.com/FutureStateofAI.html)
* [2020 Gartner Magic Quadrant for Data Science and Machine Learning Platforms](https://www.gartner.com/en/documents/3980855)
* [Gartner's 2020 Critical Capabilities for Data Science and Machine Learning Platforms](https://www.gartner.com/en/documents/3981610)
* [Forrester Now: Who’s Who In Machine-Learning Platform Vendors](https://www.forrester.com/fn/2rDQrYLmEs6jeKDfy830qO)

### Ecosystem Tools

- *Model* → Algorithm selection based on data shape and analytics doing parameter tuning, feature selection, and data selection. This includes git-focused functionality for hosting machine learning models. Examples include [JupyterHub](https://jupyter.org/hub) and [Anaconda](https://www.anaconda.com/) as well as [DVC](https://dvc.org/), [Dolt](https://github.com/dolthub/dolt), and [Delta lake](https://delta.io/).
- *Train* → Provide data teams with the tools to take raw large data sets, clean / morph / wrangle the data, and import the sanitized data into their models to prepare for deployment. Examples of this include [Trifacta](https://www.trifacta.com/), [TensorFlow Serving](https://www.kubeflow.org/docs/components/serving/tfserving_new/), [UbiOps](https://ubiops.com/features/), and [Sagemaker](https://aws.amazon.com/sagemaker/).
- *Test* → Verify everything works as expected and is ready for deployment. Examples of this include [PyTest](https://docs.pytest.org/en/stable/), [PyTorch](https://pytorch.org/), [Keras](https://keras.io/), and [Scikit-learn](https://scikit-learn.org/stable/).
- *Deploy* → Enable data teams to deploy their data models including partial rollout, partial rollback, and versioning of training data. Examples of this include [Kubeflow](https://www.kubeflow.org/) and [CML](https://cml.dev/).
