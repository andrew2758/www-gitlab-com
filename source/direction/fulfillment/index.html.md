---
layout: markdown_page
title: "Product Section Direction - Fulfillment"
description: "The Fulfillment section at GitLab focuses on supporting our customers to purchase, upgrade, downgrade, and renew paid subscriptions."
canonical_path: "/direction/fulfillment/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Last reviewed: 2024-02

## Fulfillment Section Overview

At GitLab, Fulfillment works to provide a seamless buying experience for our customers. We invest in the [order-to-cash](https://handbook.gitlab.com/handbook/company/quote-to-cash/) systems to make purchasing, activating, and managing GitLab subscriptions as easy as possible. This improves customer satisfaction and streamlines our go-to-market (GTM) processes, helping accelerate revenue growth for the company.

The Fulfillment section encompasses [four groups](#groups) and [nine categories](https://handbook.gitlab.com/handbook/product/categories/). Fulfillment collaborates closely with [Field Operations](https://handbook.gitlab.com/handbook/sales/field-operations/), [Enterprise Applications](https://handbook.gitlab.com/handbook/business-technology/enterprise-applications/), [Billing Ops](https://handbook.gitlab.com/handbook/finance/accounting/finance-ops/billing-ops/), [Support](https://handbook.gitlab.com/handbook/support/readiness/operations/), and [Data](https://handbook.gitlab.com/handbook/business-technology/data-team/).

We welcome feedback on our initiatives. If you have any thoughts or suggestions, please feel free to create a merge request to this page and assign it to `@ofernandez2` for review, or [open a Fulfillment Meta issue](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/new).

### Mission

> Provide customers with an excellent experience by making it easy for them to purchase GitLab paid subscriptions and add-ons, provision their purchase, and manage subscription changes such as increasing seat count or renewing.

GitLab paid plans offer rich feature sets that enable customers to build software faster and more securely. For the Fulfillment section, success is to make it as easy as we can for a customer to transact with GitLab and unlock the value of these rich feature sets in our paid offerings.

We add new product offerings, make our subscription management process simpler, and work to support our customers' preferred purchasing channels and payment methods. This requires investments across all interfaces where customers conduct business with GitLab. Given the breadth of countries, organization sizes, and industries that benefit from the GitLab product, we strive to be excellent at both direct transactions via our web commerce portal or our sales team, as well as sales via [Channels and Alliances](https://handbook.gitlab.com/handbook/sales/#channels--alliances).

### Impact on GitLab's addressable market

Our vision is to improve operational efficiency by providing a seamless end-to-end subscription management experience. This enables our Sales teams to spend more of their time on strategic accounts with [high LAM](https://handbook.gitlab.com/handbook/sales/field-operations/sales-systems/gtm-technical-documentation/#landed-addressable-market-lam). It also allows our Support and Finance teams to be more efficient.

We also bring new paid product offerings to market, including the FY24 addition of GitLab Duo Pro add-on and Enterprise Agile Planning. 

## Recent Accomplishments

In FY23 and FY24, we took a deliberate approach to slow down new feature development in favor of improving the foundations of our systems and addressing pain points with existing functionality. This positioned us well to launch new offerings at the end of FY24, including Enterprise Agile Planning add-on and Duo Pro. As we go into FY25, we strive to balance delivering on new business opportunities while continuing to simplify and improve our technical systems foundations. 

### Recent Pricing & Packaging Highlights

1. 2024-01 [GitLab Duo Pro add-on](/blog/2024/01/17/gitlab-duo-pro/).
2. 2023-11 [Enterprise Agile Planning add-on](/blog/2023/11/16/gitlab-enterprise-agile-planning-add-on-for-all-roles/) for GitLab Ultimate subscriptions.
3. 2023-02 [Premium tier price change](/blog/2023/03/02/gitlab-premium-update/) including automated transitional pricing for existing customers.

### Recent Customer Experience Improvements
2. 2024-01 Enabled gitlab.com Ultimate trials on namespaces with an active Premium plan (requires sales team assistance).
3. 2024-02 Revamped our [Customer Portal's subscription card](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/5317) to make it easy for subscription admins to understand their subscription and manage it. 
1. [Allow customers that purchased via Reseller to access Customer Portal](https://gitlab.com/groups/gitlab-org/-/epics/8941/), this enabled customers that purchase their GitLab subscription via a reseller or partner to view their subscription details, provision their subscription, and manage their contact information without needing to open a support ticket. 
2. GitLab.com [namespace storage visibility](https://docs.gitlab.com/ee/user/usage_quotas.html#namespace-storage-limit)
3. Updates to [Cloud Licensing](https://docs.gitlab.com/ee/administration/license.html#activate-gitlab-ee) leading to improvements in activation rates and many customers benefiting from [increased efficiency managing licenses](/pricing/licensing-faq/cloud-licensing/#why-cloud-licensing).
4. 2023-08 Released [user caps for groups](https://docs.gitlab.com/ee/user/group/manage.html#user-cap-for-groups) to all namespaces, which helps gitlab.com customers better manage their paid subscription seats.
5. Automated licensing provisioning via Cloud Licensing for [GitLab Dedicated](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/).
6. Removed logic that was preventing some customers from activating their subscriptions [related Epic](https://gitlab.com/groups/gitlab-org/-/epics/9599).
1. Made it easier to access the [Customer Portal](https://customers.gitlab.com/) via GitLab.com SSO.

## 1-year Plan

Our focus themes over the next 12 months continue to be: 

1. Enable pricing and packaging innovation at GitLab
2. Make it easier for customers to purchase from GitLab
3. Simplify our subscription model 
4. Streamline provisioning of subscriptions and trials
5. Improve subscription management
6. Drive internal team efficiency

### Enable pricing and packaging innovation at GitLab

In FY24, we added new pricing and packaging options, including [GitLab Duo Pro add-on](/blog/2024/01/17/gitlab-duo-pro/) and [Enterprise Agile Planning add-on](/blog/2023/11/16/gitlab-enterprise-agile-planning-add-on-for-all-roles/). In addition, we supported pricing changes such as the [GitLab Premium price change](/blog/2023/03/02/gitlab-premium-update/) announced on 2023-03-02.

### Make it easier for customers to purchase from GitLab

#### Enable channel partners and distributors to provide great selling motions

An increasing number of customers begin their GitLab journey via a partner. They may transact in a cloud provider's marketplace or purchase GitLab as part of a software bundle via a distributor. Our goal is to ensure those customers and partners get as high a quality of service as they would buying direct. This means extending our APIs to support indirect transactions. 

In FY25 we are building an integration with a reseller partner which will enable direct subscription orders and amendments to happen without requiring any manual order processing. We plan to launch this integration in FY25 and launch in one marketplace and, based on the success and customer feedback, continue to expand on that investment. 

For more details on this work, reference the [Fulfillment Integrations category direction](/direction/fulfillment/fulfillment-integrations-and-visibility/).

#### Improve our self-service purchase experience

Currently, we maintain separate pathways for purchases, as outlined in [this issue](https://gitlab.com/gitlab-org/fulfillment/meta/-/issues/1064). We plan to simplify by investing in a consolidated, best-in-class purchase path for our various offerings. Our investments in [using GitLab.com single sign-on as the login method for customers.gitlab.com](https://gitlab.com/groups/gitlab-org/-/epics/8905) open up the possibility for more streamlined purchase experiences across gitlab.com and self-managed plans all within our [Customer Portal](https://customers.gitlab.com/). 

For more details on this work, reference the [Subscription Management](/direction/fulfillment/subscription-management/). 

### Simplify our subscription model 

We want to take the complexity out of understanding GitLab's [pricing](https://handbook.gitlab.com/handbook/company/pricing/), so that a customer can easily understand and manage their subscription usage and how it relates to billing. A key problem area has been seat overages transparency, [seat usage visibility](https://gitlab.com/groups/gitlab-org/-/epics/5872), and customer understanding of our overages model. We are looking for ways to simplify our seat overages model in FY25 by beginning a phased launch of requiring seats to be purchased & available before additional users can be added to a group or instance. Although we will be initially intoducing this no-overages functionality to a subset of customers, we hope to over time make it the default billing model. 

As we roll out [namespace Storage limits on gitlab.com](https://docs.gitlab.com/ee/user/usage_quotas.html), we are working to ensure that [namespace storage is easy to understand](https://gitlab.com/groups/gitlab-org/-/epics/8852) and manage for all gitlab.com users.

### Streamline provisioning of subscriptions and trials

We are working on expanding and streamlining options for customers to trial new products, such as GitLab Duo Pro. 

In the future, we plan to also work on:
1. Allowing customers to manage and use a single subscription across multiple SM instances and gitlab.com namespaces.

For more details on this work, reference the [SaaS Provisioning](/direction/fulfillment/saas-provisioning/) and [Self-Managed Provisioning](/direction/fulfillment/sm-provisioning/) direction pages.

### Improve subscription management

Managing a GitLab subscription should be simple and largely automated. In order to make this a reality for all customers, we are investing in:
1. Making renewal emails more relevant, clear and actionable for the customers.
2. Adding the ability for multiple users to manage the same subscription in our Customer Portal [Epic](https://gitlab.com/groups/gitlab-org/-/epics/10495).
3. Allow a customer to add seats via the Customer Portal and pay via invoice (not requiring a credit card to pay for web direct add-ons). 

For more details on this work, reference the [Subscription Management category direction](/direction/fulfillment/subscription-management/)

### Drive internal team efficiency

GitLab team members are passionate about delivering value to our customers. We are investing in the following initiatives to better enable them to do this: 

1. [Aligning customers.gitlab.com and Zuora billing account models](https://gitlab.com/groups/gitlab-org/-/epics/8331).
1. Making key internal system integrations more robust and resilient.
1. Investing in Fulfillment developer productivity via code refactors and new tooling deployment.
1. Making it easier for [customer-facing team members to resolve licensing issues around renewals](https://gitlab.com/groups/gitlab-org/-/epics/10173). 

As we complete these investments we will reduce the complexity of our order-to-cash systems, making it easier to innovate and deliver improvements to GitLab customers and our internal stakeholders across sales, billing, and more.

For more details on this work, reference the Fulfillment Platform groups direction pages and roadmaps in [Groups](#groups).

## Roadmap

Due to the [not public](https://handbook.gitlab.com/handbook/communication/confidentiality-levels/#not-public) nature of most of our projects, our product roadmap is internal. 

We have [Fulfillment FY25 Plans and Prioritization](https://gitlab.com/gitlab-com/Product/-/issues/12843) (also Not Public), that GitLab team members can reference to track all planned initiatives by theme.

### Roadmap Prioritization

To learn more about our roadmap prioritization principles and process, please see [Fulfillment Roadmap Prioritization](https://handbook.gitlab.com/handbook/product/fulfillment-guide/#fulfillment-roadmap-prioritization)

## Groups


| Group | Description | Categories |
|------------|---------------|----------|-------|
| Subscription Management | The Subscription Management group is responsible for providing customers with an easy, informed, and reliable experience to view and manage their subscriptions, billing account details and contacts. | [Subscription Management](/direction/fulfillment/subscription-management/) |
| [Fulfillment Platform](/direction/fulfillment/platform/) | Fulfillment Platform maintains and evolves our underlying order-to-cash infrastructure, including integrations with Zuora to help accelerate our goals as a section. This group also works with internal teams to build robust systems that enable our internal, customer-facing teams better support our customers. | [CustomersDot Application](/direction/fulfillment/customers-dot-application/), [Fulfillment Admin Tooling](/direction/fulfillment/fulfillment/fulfillment-admin-tooling/), [Fulfillment Infrastructure](/direction/fulfillment/fulfillment/fulfillment-infrastructure/) |
| [Provision](/direction/fulfillment/provision/) | The Provision group is responsible for provisioning and managing licenses across self-managed and SaaS (including Cloud License activation/sync and provisioning of legacy licenses). This group is also responsible for the buying experience for customers provisioning instances through third-party distributors and marketplaces, as well as per user seat provisioning for Add ons | [SM Provisioning](/direction/fulfillment/sm-provisioning/), [SaaS Provisioning](/direction/fulfillment/saas-provisioning/), [Fulfillment Integrations & Visibility](/direction/fulfillment/fulfillment-integrations-and-visibility/) |
| Utilization | The Utilization group endeavors to capture and deliver usage data (currently focused on consumables) to internal team members, prospects, and customers so that they can make the best decision for their business needs. | [Consumables Cost Management](/direction/fulfillment/consumables-cost-management/), [Seat Cost Management](/direction/fulfillment/seat-cost-management/)  |

## OKRs

Team members can reference our [Fulfillment FY25 Q1 OKRs](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/5573) (Internal). 

We follow the [OKR (Objective and Key Results)](https://handbook.gitlab.com/handbook/company/okrs/) framework to set and track goals quarterly. The Fulfillment section OKRs are set across the entire [Quad](https://handbook.gitlab.com/handbook/handbook/product/product-processes/#pm-em-ux-and-set-quad-dris).

## Performance Indicators

Fulfillment does not track Performance Indicators at this time. While we monitor performance metrics to ensure the availability, security, and robustness of our systems, and keep to our SLOs, our goals and product plans are all tracked in OKRs. 

## Recent Accomplishments and Learnings

See [Fulfillment Recap issues](https://gitlab.com/gitlab-com/Product/-/issues/?sort=updated_desc&state=closed&label_name%5B%5D=Fulfillment%20Recap&first_page_size=20) for recaps of other recent milestone accomplishments and learnings (internal when needed).

## Key Links

1. [Fulfillment Guide](https://handbook.gitlab.com/handbook/product/fulfillment-guide/): documentation around CustomersDot Admin tools and process documentation that is not part of the [core product documentation](https://docs.gitlab.com/).
2. [Dev - Fulfillment Sub Department](https://handbook.gitlab.com/handbook/engineering/development/fulfillment/): R&D team, priorities, prioritization processes, and more.
3. [Internal Handbook - Fulfillment](https://internal.gitlab.com/handbook/product/fulfillment/): documentation that can't be in the public handbook. Minimize this to only [Not Public](https://handbook.gitlab.com/handbook/handbook/communication/confidentiality-levels/#not-public) information, such as revenue-based KPIs or sensitive project documentation.
4. [GitLab Docs Subscription Documentation](https://docs.gitlab.com/ee/subscriptions/)