---
layout: markdown_page
title: "Category Direction - CustomersDot Application"
description: "The CustomersDot Application strategy page belongs to the Fulfillment Platform group of the Fulfillment section."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

 **Last updated**: 2024-03-25

## Mission

> Within the category of the CustomersDot Application the mission is to provide solid billing infrastructure for all Fulfillment and GitLab teams. This initiative centers on building a foundation and optimizing the OTC orchestration. By strengthening these critical components, contributors will gain the confidence and efficiency needed to deliver enhancements to the CustomersDot application swiftly and reliably. This strategic effort will lay the foundation for a solid billing infrastructure that not only ensures accuracy and scalability but also promotes seamless collaboration and innovation within the Fulfillment section at GitLab.

## Overview

Within [CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com/) our customers and all relevant stakeholders are able to manage billing accounts, subscriptions, add-ons and licenses.

[CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com/) is the central application to contribute for the Fulfillment Platform group.

The Fulfillment Platform group is responsible for the following areas of CustomersDot:

- Authentication system
- Integration with Zuora
- CustomersDot admin view and functionality
- Order-to-Cash (OTC) data architecture and orchestration within CustomersDot
- Audit and compliance features
- Developer experience

The Fulfillment Platform group owns, maintains and evolves the underlying architecture and orchestration for GitLab’s order-to-cash flow (OTC).

## Target audience

The target audience of the CustomersDot application spans all stakeholder groups, as it enables them to manage subscriptions payments, customers, billing accounts, and licenses.

Therefore, we see the following parties interacting with the CustomersDot application:

- Customers (SaaS, self-managed, dedicated)
- Other Fulfillment groups and team members
- Internal stakeholders (Customer Success, Sales, Billing)

We are enhancing the overall customer experience for all our clients, whether they are self-service users, sales-assisted customers, or resellers, by optimizing infrastructure flows.

## What's up now (up to 12 months)

Within the next 12 months we want to strengthen the foundation of the fulfillment platform which includes the following focus areas:

- Improve the billing infrastructure
- Improve our order-to-cash systems and underlying data architecture
- Establish the application foundation for Fulfillment and GitLab teams

The focus will help us to support the scale within Fulfillment and adjacent areas as GitLab continues to grow.

**Current key projects for category:**

- [Zuora offline mode](https://gitlab.com/groups/gitlab-org/-/epics/11840)
- [Improve Plan / SKU management](https://gitlab.com/groups/gitlab-org/-/epics/6579)

**Roadmap:**

For a full list of our upcoming and ongoing projects for the category CustomersDot Application, check out our [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=opened&sort=end_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name[]=Fulfillment+Roadmap&label_name[]=group::fulfillment+platform&label_name[]=Category:CustomersDot+Application&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP&show_labels=false).

### Future opportunities

As we progress with our current key projects to build the underlying foundation of CustomersDot, we will identify new areas of opportunity. Future opportunities will continue to focus on Fulfillment process efficiency and internal team efficiency in relation to the CustomersDot application.

**List of opportunities we will tackle in the future:**

- [Support and enable Organizations from Fulfillment perspective and align models within data architecture](https://gitlab.com/groups/gitlab-org/-/epics/9885)
- [Align Customers Dot Orders to Zuora Subscriptions](https://gitlab.com/groups/gitlab-org/-/epics/9748) to further strengthen Zuora as Single Source of Truth (SSoT)
- [Zuora offline mode](https://gitlab.com/groups/gitlab-org/-/epics/11840)
- [Improve Plan / SKU management](https://gitlab.com/groups/gitlab-org/-/epics/6579)
- Further refinement of our [Order-2-Cash Systems Data Architecture](https://about.gitlab.com/company/quote-to-cash/#q2c-system-architecture)
- Enabling other Fulfillment groups for specific projects

## Key Links

For more details into future oportunities of other categories within the Fulfillment Platform group, please refer to the sections in the dedicated category pages:
- [Fulfillment Infrastructure](/direction/fulfillment/fulfillment-infrastructure/)
- [Fulfillment Admin Tooling](/direction/fulfillment/fulfillment-admin-tooling/)
- [Performance indicators](https://internal.gitlab.com/handbook/company/performance-indicators/product/fulfillment-section/#fulfillment-platform---general-availability-through-slis-and-error-budgets)
- [Fulfillment Platform group roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=opened&sort=start_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name[]=Fulfillment+Roadmap&label_name[]=group::fulfillment+platform&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP&show_labels=false)
