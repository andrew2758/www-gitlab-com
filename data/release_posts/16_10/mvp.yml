# Release post data file for MVP item
#
# Read through the Release Posts Handbook for more information:
# https://about.gitlab.com/handbook/marketing/blog/release-posts/#mvp
#
# DELETE THESE COMMENTS BEFORE SUBMITTING THE MERGE REQUEST
---
mvp:
  fullname: ['Lennard Sprong', 'Marco Zille']
  gitlab: ['X_Sheep', 'zillemarco']
  description: |
    [Lennard Sprong](https://gitlab.com/X_Sheep) previously won the GitLab MVP award in 15.4 and 
    was also nominated in 16.9.
    He continues to provide contributions to GitLab Workflow for VS Code, merging 8 contributions
    in the past two months.
    Some of his past contributions include the ability to [watch the trace of running CI jobs](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/merge_requests/674), 
    [view downstream pipelines](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/merge_requests/1336),
    and [compare images in merge requests](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/merge_requests/1319).
    Lennard is also actively involved in issues inside the [gitlab-vscode-extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension)
    project.

    [Erran Carey](https://gitlab.com/erran), Staff Fullstack Engineer at GitLab, nominated Lennard and
    noted that "Lennard resolved an [issue viewing pipelines](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/1000)
    affecting GitLab Community Edition users. 
    He pointed impacted users to the existing workaround before [creating a merge request](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/merge_requests/1417)
    to address the issue."

    [Tomas Vik](https://gitlab.com/viktomas), Staff Fullstack Engineer at GitLab, additionally supported Lennard and highlighted a contribution
    to [add support for image diff](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/merge_requests/1319)
    that allows people to view image changes during merge request review.
    
    [Marco Zille](https://gitlab.com/zillemarco) also wins his second GitLab MVP award, previously winning in 15.3.
    Marco was recognized not only for code contributions this release, but also for ongoing efforts supporting GitLab's wider
    community of contributors, running community pairing sessions, collaborating with GitLab team members, and
    reviewing merge requests.
    
    Marco added the ability to [cancel a pipeline immediately after one job fails](https://gitlab.com/gitlab-org/gitlab/-/issues/23605).
    The feature is enabled and available on GitLab.com but still behind a feature flag
    for self-hosted instances.
    It will be made available for everyone in 16.11.

    [Allison Browne](https://gitlab.com/allison.browne), Senior Backend Engineer at GitLab, nominated Marco for picking up this long 
    standing and highly requested feature request in pipeline execution.
    [Fabio Pitino](https://gitlab.com/fabiopitino), Principal Engineer at GitLab, added that "Marco
    not only implemented the fix but also was instrumental to the design of the feature, 
    bringing use cases and discussing them with customers interested in the feature."

    [Peter Leitzen](https://gitlab.com/splattael) additionally supported Marco's nomination by highlighting how Marco helped to [review
    and then finish a fix](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/112813#note_1737719869) 
    for loading the stack trace from Sentry.

    We are so grateful for the continued support from Lennard and Marco to improve GitLab and support our
    open source community! 🙌
