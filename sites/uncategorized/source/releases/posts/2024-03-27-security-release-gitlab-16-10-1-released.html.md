---
title: "GitLab Security Release: 16.10.1, 16.9.3, 16.8.5"
categories: releases
author: Kevin Morrison
author_gitlab: kmorrison1
author_twitter: gitlab
description: "Learn more about GitLab Security Release: 16.10.1, 16.9.3, 16.8.5 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2024/03/27/security-release-gitlab-16-10-1-released/'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 16.10.1, 16.9.3, 16.8.5 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important security fixes, and we strongly recommend that all GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version.

GitLab releases patches for vulnerabilities in dedicated security releases. There are two types of security releases:
a monthly, scheduled security release, released a week after the feature release (which deploys on the 3rd Thursday of each month),
and ad-hoc security releases for critical vulnerabilities. For more information, you can visit our [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of our regular and security release blog posts [here](/releases/categories/releases/).
In addition, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are dedicated to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest security release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Table of fixes

| Title | Severity |
| ----- | -------- |
| [Stored-XSS injected in Wiki page via Banzai pipeline](#stored-xss-injected-in-wiki-page-via-banzai-pipeline) | High |
| [DOS using crafted emojis](#dos-using-crafted-emojis) | Medium |

### Stored-XSS injected in Wiki page via Banzai pipeline

An issue has been discovered in GitLab CE/EE affecting all versions before 16.8.5, all versions starting from 16.9 before 16.9.3, all versions starting from 16.10 before 16.10.1. A wiki page with a crafted payload may lead to a Stored XSS, allowing attackers to perform arbitrary actions on behalf of victims.
This is a high severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:H/I:H/A:N`, 8.7).
It is now mitigated in the latest release and is assigned [CVE-2023-6371](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6371).

Thanks [yvvdwf](https://hackerone.com/yvvdwf) for reporting this vulnerability through our HackerOne bug bounty program.


### DOS using crafted emojis

An issue has been discovered in GitLab CE/EE affecting all versions before 16.8.5, all versions starting from 16.9 before 16.9.3, all versions starting from 16.10 before 16.10.1. It was possible for an attacker to cause a denial of service using malicious crafted description parameter for labels.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L`, 4.3).
It is now mitigated in the latest release and is assigned [CVE-2024-2818](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-2818).

Thanks Quintin Crist of Trend Micro for reporting this vulnerability to us.


### Bump PostgreSQL to 13.14, 14.11

The PostgreSQL project released an update so we are updating to versions 13.14 and 14.11.


## Non Security Patches

### 16.10.1

* [CI: bump CI_TOOLS_VERSIONS to 5.8.0 (Backport 16.10)](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1767)
* [Backport protobuf and pgx upgrades [16.10]](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/6778)
* [Fix new project group templates pagination (16-10-stable-ee)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/147552)
* [Update redis-client to v0.21.1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/147704)

### 16.9.3

* [CI: bump CI_TOOLS_VERSIONS to 5.8.0 (Backport 16.9)](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1768)
* [Backport protobuf and pgx upgrades [16.9]](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/6780)
* [Fix detect-tests CI job](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146777)
* [Collect the artifacts from the same namespace](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146820)
* [Fix new project group templates pagination (16-9-stable-ee)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/147554)
* [Backport: RSpec changes for .com handling nightly packages](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7471)

### 16.8.5

* [CI: bump CI_TOOLS_VERSIONS to 5.8.0 (Backport 16.8)](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1769)
* [Fix detect-tests CI job](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146774)
* [Backport: RSpec changes for .com handling nightly packages](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7472)
* [Backport c2a94ae8 for creating stable tag for 16-8-stable](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7479)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Security Release Notifications

To receive security release blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [security release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
